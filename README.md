# Credit Card Storage System

A credit card storage system with NodeJS, ExpressJS for the backend and API, and ReactJS for the frontend. Vite for the build system and yarn as package manager.

## Setup Instruction

Clone the repo, or use the downloaded folder. Next, move to the project folder in the command line and follow these steps.

### Initialize

```
yarn
```

### Run server

```
yarn server
```

### Run client

```
yarn dev
```

## Documentation

The application is split across two folders: server and Client. The server folder contains the code for the ExpressJS application, and the Client includes a ReactJS application which acts as the front for the application.

### Server

The server uses ExpressJS to provide two routes defined in the routes.js file. These two routes are "add" and "getall", which perform their respective actions. 

The express application uses body-parser and cors middleware to handle request body parsing and cors capabilities, respectively.


### Client

The Client is built using ReactJS and uses Vite as the build system. The application code is split into two primary components and one context to perform state management. A full-fledged state management library is not used (e.g. redux) since the requirements were simple enough that a single react context would suffice. 

The context provides the store and the two actions of add and getall, which any child components can consume. The UI is split into two parts:  List and Form. The list is the table of credit cards on record, as fetched from the server, and Form contains the form, for adding a new record. 

The Client additionally uses Axios to communicate with the server, which could be extended using custom Axios instances and request/response interceptors to implement multiple authentication methods. The component library used is mantine (https://mantine.dev).