import { Group, Table } from "@mantine/core";
import { useContext, useEffect } from "react";
import { FaCcMastercard, FaCcVisa } from "react-icons/fa";
import creditCardType from "credit-card-type";
import { DataStore } from "./DataStore";

export default ({}) => {
  const { data, loading, fetch } = useContext(DataStore);

  function cc_format(value) {
    var v = value.replace(/\s+/g, "").replace(/[^0-9]/gi, "");
    var matches = v.match(/\d{4,16}/g);
    var match = (matches && matches[0]) || "";
    var parts = [];

    for (let i = 0, len = match.length; i < len; i += 4) {
      parts.push(match.substring(i, i + 4));
    }

    if (parts.length) {
      return parts.join(" ");
    } else {
      return value;
    }
  }

  function isVisa(cardNumber) {
    const value = creditCardType(cardNumber);
    console.log(value);
    return value && value.length > 0 && value[0].type === "visa";
  }

  useEffect(() => {
    if (!loading) {
      fetch();
    }
  }, []);

  return (
    <>
      <Table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Card Number</th>
            <th>Balance</th>
            <th>Limit</th>
          </tr>
        </thead>
        <tbody>
          {data.map(({ name, cardNumber, balance, limit }) => {
            return (
              <tr key={cardNumber}>
                <td style={{ textAlign: "left" }}>{name}</td>
                <td style={{ textAlign: "left" }}>
                  <Group align="center" style={{ gap: 8 }}>
                    {isVisa(cardNumber) ? (
                      <FaCcVisa size={24}></FaCcVisa>
                    ) : (
                      <FaCcMastercard size={24}></FaCcMastercard>
                    )}
                    {cc_format(cardNumber)}
                  </Group>
                </td>
                <td style={{ textAlign: "left" }}>
                  £ {balance.toLocaleString()}
                </td>
                <td style={{ textAlign: "left" }}>£ {limit}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </>
  );
};
