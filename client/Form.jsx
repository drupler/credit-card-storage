import { Button, Card, Group, NumberInput, TextInput } from "@mantine/core";
import { useForm } from "@mantine/form";
import { showNotification } from "@mantine/notifications";
import { useContext } from "react";
import { DataStore } from "./DataStore";

const luhn = (function (arr) {
  return function (ccNum) {
    var len = ccNum.length,
      bit = 1,
      sum = 0,
      val;

    while (len) {
      val = parseInt(ccNum.charAt(--len), 10);
      sum += (bit ^= 1) ? arr[val] : val;
    }

    return sum && sum % 10 === 0;
  };
})([0, 2, 4, 6, 8, 1, 3, 5, 7, 9]);

export default ({}) => {
  const form = useForm({
    initialValues: {
      name: "",
      cardNumber: "",
      limit: 0,
    },
    validate: {
      name: (value) =>
        value && value.length > 2 && value.length < 256
          ? null
          : "Name must be between 2 and 255 characters",
      cardNumber: (value) =>
        luhn(value) ? null : "Invalid credit card number",
      limit: (value) => (value > 0 ? null : "Must be a positive number"),
    },
  });
  const { store, loading } = useContext(DataStore);
  return (
    <Card title="Add" withBorder>
      <TextInput label="Name" {...form.getInputProps("name")} required />
      <TextInput
        label="Credit Card Number"
        {...form.getInputProps("cardNumber")}
        required
      />
      <NumberInput
        label="Limit"
        {...form.getInputProps("limit")}
        required
        min={0}
      />
      <Group position="apart" mt="xl">
        <Button
          variant="white"
          onClick={() => {
            form.setValues({ name: "", cardNumber: "", limit: 0 });
          }}
        >
          Clear
        </Button>
        <Button
          variant="filled"
          onClick={async () => {
            if (!form.validate().hasErrors) {
              try {
                await store({ ...form.values, balance: 0 });
                form.setValues({ name: "", cardNumber: "", limit: 0 });
              } catch (error) {}
            }
          }}
          loading={loading}
        >
          Add
        </Button>
      </Group>
    </Card>
  );
};
