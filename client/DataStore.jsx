import { createContext, useState } from "react";
import {} from "@mantine/core";
import { showNotification } from "@mantine/notifications";
import axios from "axios";

export const DataStore = createContext({});

export default ({ children }) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  async function store(card) {
    setLoading(true);

    try {
      await axios.post("http://localhost:3000/add", card);
      setData([...data, card]);
      showNotification({
        title: "Success!",
        message: "Card added successfully",
      });
    } catch (error) {
      console.error(error);
      showNotification({
        color: "red",
        title: "Oops!",
        message: error.response.data.message,
      });
    }

    setLoading(false);
  }

  async function fetch() {
    setLoading(true);

    try {
      const response = await axios.get("http://localhost:3000/getall");
      setData(response.data);
    } catch (error) {
      console.error(error);
      showNotification({
        color: "red",
        title: "Oops!",
        message: error.response.data.message,
      });
    }

    setLoading(false);
  }

  const value = {
    data,
    setData,
    loading,
    store,
    fetch,
  };

  return <DataStore.Provider value={value}>{children}</DataStore.Provider>;
};
