import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
import {
  Grid,
  LoadingOverlay,
  MantineProvider,
  Notification,
  Paper,
  ScrollArea,
} from "@mantine/core";
import DataStore from "./DataStore";
import Form from "./Form";
import List from "./List";
import { NotificationsProvider } from "@mantine/notifications";

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className="App">
      <MantineProvider>
        <NotificationsProvider>
          <DataStore>
            <Paper
              style={{
                display: "flex",
                flexDirection: "column",
                width: "100vw",
                height: "100vh",
                position: "fixed",
                top: 0,
                left: 0,
                overflow: "hidden",
                gap: 16,
              }}
              p="lg"
            >
              <Grid>
                <Grid.Col span={4}>
                  <Form></Form>
                </Grid.Col>
              </Grid>
              <ScrollArea style={{ flexGrow: 1 }}>
                <List></List>
              </ScrollArea>
            </Paper>
          </DataStore>
        </NotificationsProvider>
      </MantineProvider>
    </div>
  );
}

export default App;
