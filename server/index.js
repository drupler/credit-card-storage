import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import routes from "./routes.js";

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(routes);
app.use("*", (request, response) => {
  response.status(404).send({ code: 404, message: "Resource not found" });
});

app.listen(3000, () => {
  console.log("Server running on port 3000");
});
