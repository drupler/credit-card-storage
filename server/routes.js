import { Router } from "express";

const data = [
  {
    cardNumber: "5399186576657329",
    name: "Jack Miller",
    balance: "1200",
    limit: "5000",
  },
  {
    cardNumber: "4532012937625444",
    name: "Bobby Mash",
    balance: "2200",
    limit: "8000",
  },
];

const router = Router();

router.get("/getall", (request, response) => {
  response.send(data);
});

router.post("/add", (request, response) => {
  let found = false;

  for (let i = 0; i < data.length; i++) {
    const item = data[i];
    if (item.cardNumber === request.body.cardNumber) {
      found = true;
      break;
    }
  }

  if (!found) {
    data.push(request.body);
    response
      .status(200)
      .send({ code: 200, message: "Credit card added successfully" });
  } else {
    response
      .status(409)
      .send({ code: 409, message: "Credit card already on file" });
  }
});

export default router;
